"""
This module contains model declarations for accounts for college project.
"""
from datetime import timedelta

from time_table.models import Semester, Lecture

from django.contrib.auth.models import AbstractUser
from django.db import models

USER_CHOICES = [
    ('professor', 'Professor'),
    ('student', 'Student')
]


class User(AbstractUser):
    """
    Inherited class to add user_type field.
    """
    user_type = models.CharField(choices=USER_CHOICES, max_length=20, blank=True)

    def save(self, *args, **kwargs):
        """
        Overrode to set password
        """
        if not self.id and not self.is_superuser:
            self.set_password(self.password)
        return super(User, self).save(*args, **kwargs)


class Professor(User):
    """
    This class contains details about the Professor.
    """
    week_working_hours = models.PositiveIntegerField(default=18)
    day_working_hours = models.PositiveIntegerField(default=4)

    class Meta:
        verbose_name = 'Professor'
        verbose_name_plural = 'Professors'

    def save(self, *args, **kwargs):
        """
        Fill the value of user_type
        """
        self.user_type = 'professor'
        return super().save(*args, **kwargs)

    def check_availability(self, lecture):
        """
        This method checks if the professor is available for the day and for
        the week.
        """
        date = lecture.start_time.date()
        week_start_date = date - timedelta(days=date.weekday())
        week_end_date = week_start_date + timedelta(days=6)
        day_lectures = Lecture.objects.filter(start_time__date=date, professor=self)
        if len(day_lectures) >= \
                self.day_working_hours:
            return f'The professor you have selected can only teach {self.day_working_hours} ' \
                   f'classes a day, please consider rescheduling this lecture.'
        elif len(Lecture.objects.filter(
                start_time__range=(week_start_date, week_end_date),
                professor=self)) >= self.week_working_hours:
            return f'The professor you have selected can only teach {self.week_working_hours} ' \
                   f'classes in a week, please consider rescheduling this lecture.'
        elif day_lectures.filter(end_time__gt=lecture.start_time,
                                 end_time__lt=lecture.end_time) or day_lectures.filter(
                                start_time__gt=lecture.start_time, start_time__lt=lecture.end_time):
            return f'This lecture overlaps with already scheduled lectures of {self}, please ' \
                   f'remedy the situation.'
        return False


class Student(User):
    """
    This class contains details about the Student.
    """
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        """
        Fill the value of user_type
        """
        self.user_type = 'student'
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'
