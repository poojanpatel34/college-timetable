from django.contrib import admin

from .models import Student, Professor, User


class ProfessorAdmin(admin.ModelAdmin):
    exclude = ['is_superuser', 'groups', 'user_permissions', 'user_type',
               'date_joined', 'last_login']

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields = ['week_working_hours', 'day_working_hours']
        return readonly_fields


class StudentAdmin(admin.ModelAdmin):
    exclude = ['is_superuser', 'groups', 'user_permissions', 'user_type',
               'date_joined', 'last_login']


class UserAdmin(admin.ModelAdmin):
    exclude = ['user_type']


admin.site.register(Professor, ProfessorAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(User, UserAdmin)
