# Generated by Django 3.1.2 on 2021-03-06 09:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('time_table', '0001_initial'),
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='semester',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='time_table.semester'),
        ),
    ]
