from . import views

from django.urls import path

app_name = 'time_table'
urlpatterns = [
    path('', views.weekly, name='index'),
    path('today/', views.daily, name='day')
]
