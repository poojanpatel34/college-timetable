from django.contrib import admin

from .models import Lecture, Semester, Subject


class LectureAdmin(admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields = ['start_time', 'end_time', 'semester']
        return readonly_fields


class SemesterAdmin(admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields = ['week_study_hours', 'daily_study_hours']
        return readonly_fields


admin.site.register(Lecture, LectureAdmin)
admin.site.register(Semester, SemesterAdmin)
admin.site.register(Subject)
