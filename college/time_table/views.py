from datetime import datetime, timedelta

from .models import Lecture

from django.shortcuts import render


def weekly(request):
    context = {}
    if request.user.is_authenticated:
        date_today = datetime.today()
        week_start_date = date_today - timedelta(days=date_today.weekday())
        week_end_date = week_start_date + timedelta(days=6)
        if request.user.user_type == 'professor':
            lectures = Lecture.objects.filter(start_time__range=(week_start_date, week_end_date),
                                              professor=request.user.professor)
            context.update({'lectures': lectures})
        elif request.user.user_type == 'student':
            lectures = Lecture.objects.filter(start_time__range=(week_start_date, week_end_date),
                                              semester=request.user.student.semester)
            context.update({'lectures': lectures})

    return render(request, 'time_table/home.html', context=context)


def daily(request):
    context = {}
    if request.user.is_authenticated:
        date_today = datetime.today()
        context.update({'date_today': date_today.strftime('%m-%d-%Y')})
        if request.user.user_type == 'professor':
            lectures = Lecture.objects.filter(start_time__date=date_today,
                                              professor=request.user.professor)
            context.update({'lectures': lectures})
        elif request.user.user_type == 'student':
            lectures = Lecture.objects.filter(start_time__date=date_today,
                                              semester=request.user.student.semester)
            context.update({'lectures': lectures})
    return render(request, 'time_table/daily.html', context=context)
