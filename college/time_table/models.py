"""
This module holds definitions of Semester, Subject and Lecture models.
"""
from datetime import timedelta

from django.core.exceptions import ValidationError
from django.db import models


class Semester(models.Model):
    """
    This class contains definition of the Semester model.
    """

    semester_number = models.PositiveIntegerField(primary_key=True)
    week_study_hours = models.PositiveIntegerField(default=25)
    daily_study_hours = models.PositiveIntegerField(default=6)

    def __str__(self):
        """
        Display name would be Sem. + semester_number
        """
        return f'Sem {self.semester_number}'

    def check_availability(self, lecture):
        """
        This method checks if the professor is available for the day and for
        the week.
        """
        date = lecture.start_time.date()
        week_start_date = date - timedelta(days=date.weekday())
        week_end_date = week_start_date + timedelta(days=6)
        day_lectures = Lecture.objects.filter(start_time__date=date, semester=self)
        if len(day_lectures) >= \
                self.daily_study_hours:
            return f'The class you have selected can only study {self.daily_study_hours} classes' \
                   f' a day, please consider rescheduling this lecture.'
        elif len(Lecture.objects.filter(
                start_time__range=(week_start_date, week_end_date),
                semester=self)) >= self.week_study_hours:
            return f'The class you have selected can only study {self.week_study_hours} classes ' \
                   f'in a week, please consider rescheduling this lecture.'
        elif day_lectures.filter(end_time__gt=lecture.start_time,
                                 end_time__lt=lecture.end_time) or day_lectures.filter(
                                start_time__gt=lecture.start_time, start_time__lt=lecture.end_time):
            return f'This lecture overlaps with already scheduled lectures of {self},' \
                   f' please remedy the situation.'
        return False


class Subject(models.Model):
    """
    This class contains definition of Subject model.
    """

    name = models.CharField(unique=True, max_length=100)

    def __str__(self):
        return self.name


class Lecture(models.Model):
    """
    This class contains definition of Lecture model.
    """
    professor = models.ForeignKey('account.Professor', on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(
        help_text="End time should be exactly one hour greater than start time")

    def __str__(self):
        return f'{self.subject} - {self.professor} - {self.semester}'

    def clean_fields(self, exclude=None):
        """
        Check if diff between start and end time = 1 hour.
        """
        diff = self.end_time - self.start_time
        if self.end_time < self.start_time:
            raise ValidationError("Start time cannot be greater than end time!")
        elif diff.seconds != 3600:
            raise ValidationError("Difference between end time and start time should be exactly "
                                  "one hour!")
        elif self.start_time.weekday() in [5, 6]:
            raise ValidationError("You cannot schedule a lecture on weekends!")
        else:
            class_availability = self.semester.check_availability(self)
            professor_availability = self.professor.check_availability(self)
            if class_availability:
                raise ValidationError(class_availability)
            if professor_availability:
                raise ValidationError(professor_availability)
            return True
